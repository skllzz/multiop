package multiop

import (
	"context"
	"fmt"
	"sync/atomic"
	"testing"
	"time"
)

func TestMultiOpError(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	runner := NewParallelRunner(ctx, 1)
	er := fmt.Errorf("expected")
	for i := 0; i < 5000; i++ {
		if err := runner.Submit(func(ctx context.Context) error {
			return er
		}); err != nil {
			if err != er {
				t.Fatal("not expected error")
			}
			break
		}
	}
	if err := runner.Await(); err != nil {
		if err != er {
			t.Fatal("not expected error")
		}
	} else {
		t.Fatal("expecting error")
	}
	cancel()
}
func TestMultiOp(t *testing.T) {
	//ctx, _ := context.WithTimeout(context.Background(), 1170*time.Millisecond)
	ctx := context.Background()
	runner := NewParallelRunner(ctx, 10)
	scnt := int64(0)
	rcnt := int64(0)
	for c := 0; c < 10000; c++ {
		for i := 0; i < 50; i++ {
			n := i
			atomic.AddInt64(&scnt, 1)
			if runner.Submit(func(ctx context.Context) error {
				atomic.AddInt64(&rcnt, 1)
				//t.Logf("Running %v", n)
				tmr := time.NewTimer(1 * time.Microsecond)
				defer tmr.Stop()
				select {
				case <-tmr.C:
				case <-ctx.Done():
					return ctx.Err()
				}

				if n > 55 {
					//return errors.New("fail")
				}

				return nil
			}) != nil {
				break
			}
		}

		if e := runner.Await(); e != nil {
			t.Fatal(e)
		}
		runner.Restart()
	}
	runner.Dispose()
	if rcnt != scnt {
		t.Fatalf("%v!=%v", rcnt, scnt)
	}
}
