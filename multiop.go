package multiop

import (
	"context"
	"go.uber.org/zap"
	"golang.org/x/sync/semaphore"

	"sync"
	"sync/atomic"
	"time"
)

var logger *zap.Logger
var _concurencyLimit = "_concurencyLimit"
var defaultConcurrency = 16
var maxConcurrency = 0

type RunnerPool struct {
	awaitCtx   context.Context
	ctx        context.Context
	done       chan error
	firstError error
	functions  chan func(context.Context) error

	awaiting    *semaphore.Weighted
	submitted   int64
	completed   int64
	concurrency int
	cancel      context.CancelFunc
	start       time.Time
	elapsed     time.Duration
}

// SetLogger устанавливает logger для вывода данных ошибки в случае recover()
func SetLogger(instance *zap.Logger) {
	logger = instance
}

// SetGlobalMaxConcurrency задает максимальное ограничение параллельности новых раннеров (0 - не ограничивать)
func SetGlobalMaxConcurrency(max int) {
	maxConcurrency = max
}

// NewParallelRunner создание пула для выполнения функций с заданным уровнем параллелизма
func NewParallelRunner(ctx context.Context, concurrency int) *RunnerPool {
	res := new(RunnerPool)
	res.ctx = ctx
	res.awaiting = semaphore.NewWeighted(1)
	res.SetConcurrency(concurrency)
	res.Restart()
	res.start = time.Now()
	return res
}

// Submit добавление в пул функции для выполнения, первая функция вернувшая ошибку останавливает пул и Await вернет эту ошибку
func (rp *RunnerPool) Submit(block func(context.Context) error) error {
	if e := rp.awaitCtx.Err(); e == nil {
		atomic.AddInt64(&rp.submitted, 1)
		select {
		case rp.functions <- block:
			return nil
		case <-rp.awaitCtx.Done():
			er := rp.firstError
			if er != nil {
				return er
			}
			return rp.awaitCtx.Err()
		}
	} else {
		er := rp.firstError
		if er != nil {
			return er
		}
		return e
	}
}

// Dispose останавливает все задания в пуле и освобождает ресурсы
func (rp *RunnerPool) Dispose() {
	rp.cancel()
}

// Await ожидает завершения выполнения всех заданий в пуле и возвращает ошибку если такая случилась
func (rp *RunnerPool) Await() error {
	rp.awaiting.Release(1)
	select {
	case e := <-rp.done:
		if e == nil {
			e = rp.firstError
		}
		if e == nil {
			rp.elapsed = time.Since(rp.start)
		}
		return e
	case <-rp.ctx.Done():
		er := rp.firstError
		if er != nil {
			return er
		}
		return rp.ctx.Err()
	}

}

// Elapsed возвращает суммарное время выполнения всех заданий для последнего Await
func (rp *RunnerPool) Elapsed() time.Duration {
	return rp.elapsed
}

// SetConcurrency меняет уровень конкурентности заданий (сработает на следующем Restart)
func (rp *RunnerPool) SetConcurrency(concurrency int) {
	if concurrency <= 0 {
		concurrency = defaultConcurrency
	}
	if maxConcurrency > 0 && concurrency >= maxConcurrency {
		if logger != nil {
			logger.Warn("Limit runner concurrency", zap.Int("required", concurrency), zap.Int("limit", maxConcurrency))
		}
		concurrency = maxConcurrency
	}
	rp.concurrency = concurrency
}

// GetConcurrency получить текущий уровень конкурентности заданий
func (rp *RunnerPool) GetConcurrency() int {
	return rp.concurrency
}

// Restart перезапускает пул после Await чтобы можно было повторно не создавать новый, а пользоваться несколько раз одним и тем же
func (rp *RunnerPool) Restart() {
	ocancel := rp.cancel
	if ocancel != nil {
		ocancel()
	}
	if err := rp.awaiting.Acquire(rp.ctx, 1); err != nil {
		return
	}
	nctx, ncancel := context.WithCancel(rp.ctx)
	rp.awaitCtx = nctx
	rp.cancel = ncancel
	rp.firstError = nil
	rp.done = make(chan error)
	rp.functions = make(chan func(context.Context) error, rp.concurrency)
	go func() {
		pending := sync.WaitGroup{}
		for i := 0; i < rp.concurrency; i++ {
			pending.Add(1)
			go func() {
				defer func() {
					if r := recover(); r != nil {
						ncancel()
						if logger != nil {
							logger.WithOptions(zap.AddCallerSkip(4)).Error("Multiop error", zap.Any("error", r))
						}
						if err, ok := r.(error); ok {
							select {
							case rp.done <- err:
							case <-rp.ctx.Done():
							}
						}
					}
					pending.Done()
				}()
				for nctx.Err() == nil {
					select {
					case block := <-rp.functions:
						if block == nil {
							return
						}
						err := block(nctx)
						atomic.AddInt64(&rp.completed, 1)
						if err != nil && nctx.Err() == nil {
							rp.firstError = err
							ncancel()
							select {
							case rp.done <- err:
							case <-rp.ctx.Done():
							}
							return
						}
					case <-nctx.Done():
						return
					}
				}
			}()
		}
		if err := rp.awaiting.Acquire(rp.ctx, 1); err != nil {
			ncancel()
			return
		}
		close(rp.functions)
		pending.Wait()
		close(rp.done)
		rp.awaiting.Release(1)
	}()
}

// WithConcurrencyLimit установить в контексте предел параллельности операций для RunParallelOps
func WithConcurrencyLimit(ctx context.Context, limit int) context.Context {
	return context.WithValue(ctx, _concurencyLimit, limit)
}

// RunParallelOps обертка над NewParallelRunner в который через Submit передаются все функции из operations и далее ожидается их завершение через Await
func RunParallelOps(ctx context.Context, operations ...func(ctx context.Context) error) error {
	if len(operations) == 0 {
		return nil
	}
	limit := 64
	if v, ok := ctx.Value(_concurencyLimit).(int); ok && v > 0 {
		limit = v
	}
	runner := NewParallelRunner(ctx, limit)
	defer runner.Dispose()
	for _, op := range operations {
		if runner.Submit(op) != nil {
			break
		}
	}
	return runner.Await()
}
